<?php

include "maysam1.php" ;

/*
 * inde.php
 * 
 * Copyright 2017 meisam.moh <meisam.moh@WINSOLA-2BLCU7D>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>        تمرینهای php      </title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 1.27" />
</head>

<body>
	
<?php 
echo "hello word <br />";
echo "hello word <br />";
$ma1 = "hello";
$ma2 = "word";
$ma3 = $ma1." " .$ma2 ;
echo $ma3 ;
?>
<br />
<?php 
echo "$ma3 agin <br />";  
echo  2+3/2*4 ;
echo " <br />";
$ma4 = true ;
var_dump($ma4);
echo " <br />";
echo '$ma4 ma';
?>
<?php  
echo " <br />";
$s1="maysam  ";
$s2="mohammadi";
$n3=$s1;
echo $n3;
$n3.= $s2;
echo $n3;
echo "<br  />";
echo 1+"2maysam";
echo " <br />";
$m1=10;
$m1++;
echo $m1,"<br />";
$m1--;echo "maysam $m1  mohammadi","<br />","int yes/no:",is_int($m1);
?>
<?php
$r1= array("maysam","ali","hesan","reza","hesan",array(10,20,30,40));
echo "<br />",$r1[0],"<br />";
?>

	
<?php echo "<pre>";echo print_r($r1),"<br />";
echo $r1[0][4];//حرف یک ارایه را چاپ می کند //
$r1[1]=10;$r1[2]=20;
echo "<pre>";echo print_r($r1),"<br />";
$t=array("LAST_NAME" => "maysam");
echo $t["LAST_NAME"];
$ra2=array(10,20,30,15,19,35,40,44);
rsort($ra2);
echo "<pre>";
echo print_r($ra2),"<br />";
sort($ra2);echo print_r($ra2),"<br />";



$maysam[0]["name"]="maysam";
$maysam[0]["family"]="mohammadi";
$maysam[0]["age"]=15;
$maysam[1]["name"]="ali";
$maysam[1]["family"]="moahammadi";
$maysam[1]["age"]=20;
$maysam[2]["name"]="reza";
$maysam[2]["family"]="amenye";
$maysam[2]["age"]=35;
$maysam[3]["name"]="hasan";
$maysam[3]["family"]="mohammadi";
$maysam[3]["age"]=25;
var_dump($maysam);

foreach($maysam as $key=>$value){
	foreach($value as $key => $value){
		echo "$key:$value","<br>";

}
}

?>


</body>


</html>
